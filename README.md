# Uploadify
> A wheel-seminar example application: **Simple file uploader**

## Usage
* Build: `npm run build` to build the application.  
* Serve: Serve `build` directory as the web root.
  You should reverse-proxy `api/`, `downloads/` to the backend.
