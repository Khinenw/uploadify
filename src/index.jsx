import { css } from "@emotion/react";
import { setToken } from "./utils/api";
import { useState } from "react";

import React from "react";
import ReactDOM from 'react-dom';
import Main from "./pages/Main.jsx";
import Login from "./pages/Login.jsx";

import './index.css';

const App = () => {
	const [ loggedIn, setLoggedIn ] = useState(false);
	return (
		<div>
			{ loggedIn ? (
				<Main />
			) : (
				<Login onLogin={ () => setLoggedIn(true) } />
			) }
		</div>
	);
};

ReactDOM.render(
	<React.StrictMode>
		<App/>
	</React.StrictMode>,
	document.getElementById('app')
);

if (import.meta.hot) {
	import.meta.hot.accept();
}
