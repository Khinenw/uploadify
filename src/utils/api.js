import axios from "axios";

const api = axios.create({
	validateStatus: status => status < 500
});

let token = '';
api.interceptors.request.use(config => {
	config.headers.Authorization = token;
	return config;
});


export default api;
export function setToken(newToken) { token = newToken; }
