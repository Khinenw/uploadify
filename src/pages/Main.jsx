import api from "../utils/api";
import styled from "@emotion/styled";
import { css } from "@emotion/react";
import { useEffect, useState } from "react";

import FileItem from "../components/FileItem.jsx";
import UploadButton from "../components/UploadButton.jsx";

const Header = styled.header`
	background: #eaeaea;
	padding: 0 50px;
`;

const HeaderItems = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	height: 50px;
	max-width: 1280px;
	margin: 0 auto;

	h1 {
		color: #202020;
		font-family: var(--font-display);
		font-size: 1.3rem;
		font-weight: 400;
		margin: 0;
		letter-spacing: -0.04em;
	}
`;

const FileList = styled.div`
	display: flex;
	flex-direction: column;
	max-width: 1280px;
	margin: 0 auto;
	margin-top: 20px;
	padding: 0 20px;
`;

const Main = () => {
	const [ files, setFiles ] = useState(null);

	useEffect(async () => {
		const { data: { ok, reason, files } } = await api.get('/api/files/list');

		if (!ok) {
			alert(`Failed to retrieve files!\nReason: ${reason}`);
			return;
		}

		setFiles(files);
	}, []);

	const onUpload = ({ ok, reason, file }) => {
		if (!ok) {
			alert(`Failed to upload files!\nReason: ${reason}`);
			return;
		}

		setFiles(files.concat(file));
	};

	return (
		<main>
			<Header>
				<HeaderItems>
					<h1>Uploadify</h1>
					<UploadButton onUpload={ onUpload }></UploadButton>
				</HeaderItems>
			</Header>

			<FileList>
				<h1 css={ css`
					color: #202020;
					font-size: 1.4rem;
					font-weight: 400;
				` }>
					Uploaded Files
				</h1>

				{ files && files.map(file => (<FileItem file={ file } key={ file.name } />)) }
			</FileList>
		</main>
	);
};

export default Main;
