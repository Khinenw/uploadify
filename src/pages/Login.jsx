import { setToken } from "../utils/api";
import { useState } from "react";

import api from "../utils/api";
import styled from "@emotion/styled";

const LoginPage = styled.main`
	width: 100%;
	height: 100%;
	min-height: 100vh;

	display: flex;
	justify-content: center;
	align-items: center;
`;

const LoginForm = styled.form`
	display: flex;
	flex: 1;
	flex-direction: column;
	max-width: 300px;
`;

const LoginInput = styled.input`
	border-radius: 5px;
	border: 1px solid #c0c0c0;
	background: #e8e8e8;
	outline: none;
	padding: 15px 20px;
	margin: 10px 0;
	transition: border .4s ease;

	&:focus {
		border-color: var(--theme-400);
	}
`;

const LoginButton = styled.button`
	background: var(--theme-400);
	position: relative;
	margin-top: 30px;
	padding: 15px 10px;
	border: none;
	border-radius: 5px;
	outline: none;
	cursor: pointer;
	font-size: 1.2rem;
	color: #fff;
	transition: background .4s ease;

	&:hover {
		background: var(--theme-500);
	}
`;

const Login = ({ onLogin }) => {
	const [ username, setUsername ] = useState('');
	const onUsernameChange = event => setUsername(event.target.value);

	const [ password, setPassword ] = useState('');
	const onPasswordChange = event => setPassword(event.target.value);

	const login = async event => {
		event.preventDefault();

		const { data: { ok, reason, token } } = await api.post('/api/auth/login', {
			username,
			password
		});

		if (!ok) {
			alert(`Failed to login!\nReason: ${reason}`);
			return;
		}

		setToken(token);
		onLogin();
	};

	return (
		<LoginPage>
			<LoginForm onSubmit={ login }>
				<LoginInput type="text" value={ username } onChange={ onUsernameChange } placeholder="Username"/>
				<LoginInput type="password" value={ password } onChange={ onPasswordChange } placeholder="Password"/>

				<LoginButton type="submit">
					Login
				</LoginButton>
			</LoginForm>
		</LoginPage>
	);
};

export default Login;
