import api from "../utils/api";
import { css } from "@emotion/react";
import { useRef, useState } from "react";

import IconUpload from "./images/IconUpload.svg";
import ProgressBar from "./ProgressBar.jsx";

const UploadButton = ({ onUpload }) => {
	const [ progress, setProgress ] = useState(0);
	const [ isUploading, setIsUploading ] = useState(false);

	const uploadFile = async event => {
		const form = new FormData();
		form.append('file', event.target.files[0]);

		setProgress(0);
		setIsUploading(true);

		const { data } = await api.post('/api/files/upload', form, {
			headers: {
				'Content-Type': 'multipart/form-data'
			},

			onUploadProgress(event) {
				if (event.lengthComputable) {
					const total = event.total + 0.001;
					const uploaded = event.loaded;

					setProgress(uploaded / total * 100);
				}
			}
		});

		setIsUploading(false);
		onUpload(data);
	};

	return (
		<>
			{ isUploading && (
				<ProgressBar progress={ progress } />
			) }

			<label css={ css`
				cursor: pointer;
				border: none;
				background: transparent;
				color: #202020;
				transition: all .4s ease;

				&:hover {
					color: var(--theme-400);
				}
			` }>
				<IconUpload css={ css`
					height: 1.5rem;
				` } />

				<input type="file" css={ css`
					display: none;
				` } onChange={ uploadFile } />
			</label>
		</>
	);
};

export default UploadButton;
