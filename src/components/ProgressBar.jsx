import { css } from "@emotion/react";

const ProgressBar = ({ progress }) => {
	return (
		<div css={ css`
			position: fixed;
			top: 0;
			left: 0;
			width: ${progress}%;
			height: 5px;
			background: var(--theme-400);
		` }></div>
	);
};

export default ProgressBar;
