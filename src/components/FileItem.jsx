import api from "../utils/api";
import styled from "@emotion/styled";
import { css } from "@emotion/react";

import IconFile from "./images/IconFile.svg";

const FileContainer = styled.button`
	color: #202020;
	display: flex;
	align-items: center;
	margin: 10px 0;
	padding: 10px;
	border-radius: 10px;
	border: none;
	outline: none;
	background: transparent;
	text-align: left;
	cursor: pointer;
	transition: background .4s ease;

	&:hover {
		background: #f0f0f0;
	}
`;

const FileDescription = styled.div`
	display: flex;
	flex-direction: column;
`;

const FileName = styled.h2`
	margin: 0;
	font-size: 1.2rem;
	font-family: var(--font-display);
`;

const FileAuthor = styled.span`
	font-size: .9rem;
`;

const FileItem = ({ file }) => {
	const { id, name, author } = file;
	const download = async () => {
		const { data: { ok, reason, token } } = await api.get(`/api/files/download/${id}`);
		if (!ok) {
			alert(`Failed to get download token!\nReason: ${reason}`);
			return;
		}

		const link = document.createElement('a');
		link.href = `/downloads/${id}?token=${encodeURIComponent(token)}`;
		link.target = '_blank';
		link.click();
	};

	return (
		<FileContainer onClick={ download }>
			<IconFile css={ css`
				height: 32px;
				padding: 10px;
				margin-right: 15px;
			` } />
			<FileDescription>
				<FileName>{ name }</FileName>
				<FileAuthor>by { author }</FileAuthor>
			</FileDescription>
		</FileContainer>
	);
};

export default FileItem;
