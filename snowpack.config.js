const httpProxy = require('http-proxy');

const proxy = httpProxy.createServer({ target: 'http://localhost:3008' });
proxy.on('error', function (err, req, res) {
	res.writeHead(500, {
	  'Content-Type': 'application/json'
	});

	res.end('{"ok":false,"reason":"server-is-down"}');
});

const proxyWeb = (...args) => proxy.web(...args);

module.exports = {
	mount: {
		public: {url: '/', static: true},
		src: {url: '/dist'},
	},

	plugins: [
		'@snowpack/plugin-webpack',
		'@snowpack/plugin-react-refresh',
		'@snowpack/plugin-babel',
		[ 'snowpack-plugin-svgr', {
			include: [ 'src/components/images/**/*.svg' ],
			svgrOptions: { svgo: false }
		} ]
	],

	routes: [
		{ src: '/api/.*', dest: proxyWeb },
		{ src: '/downloads/.*', dest: proxyWeb },
	],

	optimize: {},

	packageOptions: {
		knownEntrypoints: [
			'@emotion/react/jsx-runtime',
			'@emotion/styled/base'
		]
	},

	devOptions: {},

	buildOptions: {}
};
